fn main() {
    let t2 = vec![981];
    let r2 = bfsearch(t2,vec![1], 10);
    println!("r2 = {:?}", r2);
    return;

    let t3 = vec![43, 981];
    let r3 = bfsearch(t3,vec![1], 8);
    println!("r3 = {:?}", r3);

    return;
    let t1 = vec![307, 439];
    let r1 = bfsearch(t1,vec![1], 8);
    println!("r1 = {:?}", r1);


    return;
}

/* https://www.inesc-id.pt/ficheiros/publicacoes/5228.pdf */
fn bfsearch(t: Vec<i32>, r: Vec<i32>, bw: usize) -> Vec<i32> {
    //let (r, t) = synthesize(vec![1], t);
    /* if T is empty return R */
    if t.len() == 0 { return r };
    println!("remaining targets for breadth-first: {:?}", t);

    /* wr <- r, wt <- t */
    let mut wr: Vec<Vec<i32>> = Vec::new();
    let mut wt: Vec<Vec<i32>> = Vec::new();
    wr.push(r.clone());
    wt.push(t.clone());

    let mut xr: Vec<Vec<i32>> = Vec::new();
    let mut xt: Vec<Vec<i32>> = Vec::new();

    println!("wr{}={:?} wt{}={:?}", 1, wr, 1, wt);

    /* n and m are the set sizes - 1 */
    let mut n = 1;
    /* while 1 do */
    loop {
        let m = n;

        /* store previous sets */
        xr = wr.clone();
        xt = wt.clone();
        assert!(xr.len() == xt.len());

        println!("xr={:?} xt={:?}, m={}", xr, xt, m);
        /* empty working sets */
        n = 0;
        wr = Vec::new();
        wt = Vec::new();
        /* for each existing ready set, create new sets with one new number */
        println!("Extending m = {} ready sets, each with one different intermediate.\n", m);
        for i in 1..=m {
            /* only consider odd numbers */
            let mut j = 1;
            while j < u64::pow(2u64, bw as u32 + 1) as i32 {
                print!("Is {} in xr{} = {:?}, or {} in xt{} = {:?}? ", j, i, xr[i-1], j, i, xt[i-1]);
                /* j not in xri and j not in xti? */
                if (xr[i-1].contains(&j) == false) && (xt[i-1].contains(&j) == false) {
                    println!("No. Try if {} helps:", j);
                    let j_singleton = vec![j];
                    let (_, b) = synthesize(xr[i-1].clone(), j_singleton.clone(), bw);
                    if b.len() == 0 {
                        /* modification to (wrong?) algorithm: do not modify xri, as j would end up
                         * in all subsequent sets on this depth. I.e. the modified lines are:
                         *
                         * 15: V <- XRi v {j}
                         * 17: = Synthesize(V, XTi);
                         */
                        let mut vec = xr[i-1].clone();
                        vec.push(j);
                        vec.sort();
                        vec.dedup();

                        println!("Yes, added j={} to xr{} = {:?}", j, i, vec/*xr[i-1]*/);
                        n = n + 1;
                        println!("for n = {}", n);
                        /* (wr, wt) = ... not yet supported currently, the use of temp1, temp2 is identical */
                        let (temp1, temp2) = synthesize(vec/*xr[i-1].clone()*/, xt[i-1].clone(), bw);
                        //println!("added {:?} to wr={:?}, added {:?} to wt={:?}", temp1, wr, temp2, wt);
                        wr.push(temp1);
                        wt.push(temp2);
                        assert!(wr.len() == wt.len());
                        print!("wr1,wt1...wrn,wtn =");
                        for i in 0..wr.len() - 1 {
                            print!("{}:{:?},{:?} ",i, wr[i], wt[i]);
                        }
                        println!("");
                        if wt[n-1].len() == 0 {
                            println!("wtn empty, returning wrn");
                            return wr[n-1].clone();
                        }
                    }
                } else {
                    println!("Yes, already present.");
                }
                j += 2;
            }
        }
    }
    //return t;
}

/* http://spiral.ece.cmu.edu/pubs/synth.pdf */
/* http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.101.8464&rep=rep1&type=pdf */

/* synthesize numbers in t with shifted add/subtract from number in r */
/* return the set of base numbers and intermediate and result numbers */
fn synthesize(mut r: Vec<i32>, mut t: Vec<i32>, bw: usize) -> (Vec<i32>, Vec<i32>) {
    loop {
        let mut isadded = false;
        let mut k = 0;
        while k < t.len() {
            println!("Can we synthesize {} with {:?}?", t[k], r);
            if synthesizable(&r, t[k], bw) {
                print!("Yes, we can synthesize {} with r = {:?}", t[k], r);
                isadded = true;
                r.push(t[k]);
                println!(", move {} to r = {:?}.", t[k], r);
                /* needed for initial case, prevents adding 1 to [1] */
                r.sort();
                r.dedup();
                t.remove(k);
            } else {
                println!("No, we can not synthesize {} with {:?} within the constraints.", t[k], r);
                k += 1;
            }
        }
        if isadded == false { break; }
    }
    println!("r, t = {:?}, {:?}", r, t);
    return (r, t);
}

/* check if with the set and adders/subtractors, tk can be implemented */
fn synthesizable(r: &Vec<i32>, tk: i32, bw: usize) -> bool {

    let mut is_synthesizable = false;
    let mut best_cost = bw as u32 * 2;

    let mut best_combination: Vec<i8> = vec![0; r.len() * bw];

    /* 1 operation is negating, i.e. b = 0 - a */
    /* 2 operation is adding/subtracting 2 numbers, etc. */
    for bits in 1..=2 {
        let mut v = subset_create(r.len() * bw, bits);
        loop {
            //println!("operations vector = {:?}", v);
            let (total, cost) = synthesize_try(&r, &v, bw, best_cost, false);
            if cost <= best_cost && total == tk as i64 {
                /* a solution is found */
                is_synthesizable = true;
                /* print solution */
                //let (_ , cost) = synthesize_try(&r, &v, best_cost, true);
                if cost < best_cost {
                    best_cost = cost;
                    best_combination = v.clone();
                    if cost == 1 { break; }
                }
            }
            /* next permutation */
            let success = subset_next(&mut v);
            /* no further permutations with this amount of operations */
            if !success { break; }
        }
        /* do not consider more operations, assuming more expensive */
        if is_synthesizable { break; }
    }
    if is_synthesizable { 
        let (_ , _) = synthesize_try(&r, &best_combination, bw, best_cost, true);
    }
    return is_synthesizable;
}

fn synthesize_try(r: &Vec<i32>, v: &Vec<i8>, bw: usize, max_cost: u32, dump: bool) -> (i64, u32) {
    let length = r.len();

    let mut total: i64 = 0;
    let mut any_partipication = false;
    let mut num_adders = 0;
    let mut num_subtractors = 0;

    let mut cost = 0;
    let mut is_first_addition = true;

    for number in 0..length * bw {
        if v[number] == 1 {
            /* print */
            if dump {
                if num_adders > 0 || num_subtractors > 0 {
                    print!("+");
                }
                print!("({}<<{})", r[number / bw ], number % bw);
            }
            /* add number */
            total += (r[number/bw] as i64) << (number % bw);
            any_partipication = true;
            num_adders += 1;

            /* add cost; first positive number added is free */
            if is_first_addition {
                is_first_addition = false;
            } else {
                cost += 1;
            }
        } else if v[number] == -1 {
            if dump { 
                print!("-({}<<{})", r[number / bw], number % bw);
            }
            total -= (r[number/bw] as i64) << (number % bw);
            any_partipication = true;
            num_subtractors += 1;

            /* add cost */
            cost += 1;
        }
        /* too expensive? */
        if cost > max_cost { 
            /* do not consider */
            return (0, cost);
        }
    }
    /* print total */
    if dump && any_partipication {
        print!("={}, cost={}", total, cost);
        println!("");
    }
    return (total, cost);
}

fn subset_next_index(operations: &mut Vec<i8>) -> bool {
    let mut index = 0;
    loop {
        /* find next operation */
        while index < operations.len() && operations[index] == 0 {
            index += 1;
        }
        if index >= (operations.len() - 1) {
            return false;
        }
        /* can we shift this operation left? */
        if operations[index + 1] == 0 {
            operations[index] = 0;
            operations[index + 1] = 1;
            return true;
        } else {
            //println!("cannot shift index {}", index);
            if (index > 0) && (operations[index - 1] == 0) {
                operations[index] = 0;
                let mut index2 = index;
                while index2 > 0 && operations[index2 - 1] == 0 {
                    index2 -= 1;
                }
                operations[index2] = 1;
            }
            index += 1;
        }
    }
}

fn subset_next(operations: &mut Vec<i8>) -> bool {
  let mut index = 0;
  loop {
    /* find first number */
    while index < operations.len() && operations[index] == 0 {
        index += 1;
    }
    if index == operations.len() {
        //println!("proceed_index");
        return subset_next_index(operations);
    }
    if operations[index] == 1 {
        operations[index] = -1;
        return true;
    } else if operations[index] == -1 {
        operations[index] = 1;
        index += 1;
    }
  }
}

fn subset_create(len: usize, bits: usize) -> Vec<i8> {
    assert!(bits <= len);
    let mut operations: Vec<i8> = vec![0; len];
    for bit in 0..bits {
        operations[bit] = 1;
    }
    return operations;
}
